# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

Generated by [`auto-changelog`](https://github.com/CookPete/auto-changelog).

## [1.1.0](https://gitlab.com/opencontent/stanzadelcittadino-parix-api/compare/1.0.1...1.1.0)

### Commits

- Added version 2 for find endpoint [`f97ebe9`](https://gitlab.com/opencontent/stanzadelcittadino-parix-api/commit/f97ebe99e5e25616e0b052b84d964b88d43b49fd)
- Small fixes [`1526868`](https://gitlab.com/opencontent/stanzadelcittadino-parix-api/commit/1526868bd5e5bc7bc24d06ac33dfbb37ab39b36d)

## [1.0.1](https://gitlab.com/opencontent/stanzadelcittadino-parix-api/compare/1.0.0...1.0.1) - 2020-11-16

### Commits

- Release 1.0.1 [`68947cf`](https://gitlab.com/opencontent/stanzadelcittadino-parix-api/commit/68947cf90c5eb35094f88d422607037b844a9de7)
- Fixed fetch error on multiple rea subscription [`e8706ee`](https://gitlab.com/opencontent/stanzadelcittadino-parix-api/commit/e8706eedd1fd61a44225ee085e6ea52414e3274b)

## 1.0.0 - 2020-10-28

### Commits

- First commit [`67f3df0`](https://gitlab.com/opencontent/stanzadelcittadino-parix-api/commit/67f3df047f5ac5b64ea48897100b1eb1687f89ed)
- Added ci [`0e493dc`](https://gitlab.com/opencontent/stanzadelcittadino-parix-api/commit/0e493dc3bba20d988f0d95483498988d60bceeac)
- Release 1.0.0 [`69698a0`](https://gitlab.com/opencontent/stanzadelcittadino-parix-api/commit/69698a038864a71f79b12df7b76e3567c702d1be)
