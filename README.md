# PARIX REST API

API to obtain information relating to a company given his fiscal code

## Installation

    composer install
     
### Configuration

It is necessary to define some environment variables before running the application:
 
| Name                    |   Description                                                                         | Required  |
|-------------------------|---------------------------------------------------------------------------------------|-----------|
|   WSDL_URL              |   Url called for soap requests                                                        |   Yes     |
|   CERTIFICATE           |   Certificate used for soap requests                                                  |   Yes     |
|   CERTIFICATE_PASSWORD  |   Certtificate's password                                                             |   Yes     |
|   USERNAME              |   Username used to authenticate soap requests                                         |   Yes     |
|   PASSWORD              |   Password used to authenticate soap requests                                         |   Yes     |


## /

Route to check if system is alive

Request `GET /`
 
## Find

This endpoint is deprecated, use v2

Method for the recovery data of the company by fiscal code `fiscal_code`, return an object

Request `GET /find/?fiscal_code=<fiscal_code>&complete=true`

Params:

`fiscal_code: fiscal code of the company`

`complete:    parameter to switch to complete or reduced detail, defaul is false`

## Find v2

Method for the recovery data of the company by fiscal code `fiscal_code`, return an array

Request `GET /v2/find/?fiscal_code=<fiscal_code>&complete=true`

Params:

`fiscal_code: fiscal code of the company`

`complete:    parameter to switch to complete or reduced detail, defaul is false`
