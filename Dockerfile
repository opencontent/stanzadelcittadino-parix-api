FROM abiosoft/caddy:1.0.3-php

RUN apk add --no-cache \
  php7-soap

COPY ./src /srv
COPY Caddyfile /etc/Caddyfile

WORKDIR /srv

RUN composer install --no-scripts --prefer-dist --no-suggest
