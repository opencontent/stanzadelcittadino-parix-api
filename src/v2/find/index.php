<?php

require '../../vendor/autoload.php';

use App\ParixRequest;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

$request = Request::createFromGlobals();

$endpoint    = getenv('WS_URL');
$wsdl        = $endpoint . '?wsdl';
$certificate = dirname(__FILE__) . '/../../certificates/' . getenv('CERTIFICATE');

$certificatePassword    = getenv('CERTIFICATE_PASSWORD');
$username    = getenv('USERNAME');
$password         = getenv('PASSWORD');


if (!$endpoint) {
  $response = new JsonResponse(['WS_URL è un parametro obbligatorio'], Response::HTTP_INTERNAL_SERVER_ERROR);
  $response->send();
}

if (!$certificate) {
  $response = new JsonResponse(['CERTIFICATE è un parametro obbligatorio'], Response::HTTP_INTERNAL_SERVER_ERROR);
  $response->send();
}

if (!$certificatePassword) {
  $response = new JsonResponse(['CERTIFICATE_PASSWORD è un parametro obbligatorio'], Response::HTTP_INTERNAL_SERVER_ERROR);
  $response->send();
}

if (!$username) {
  $response = new JsonResponse(['USERNAME è un parametro obbligatorio'], Response::HTTP_INTERNAL_SERVER_ERROR);
  $response->send();
}

if (!$password) {
  $response = new JsonResponse(['PASSWORD è un parametro obbligatorio'], Response::HTTP_INTERNAL_SERVER_ERROR);
  $response->send();
}

$fiscalCode = $request->query->get('fiscal_code', false);
if (!$fiscalCode) {
  $response = new JsonResponse(['fiscal_code è un parametro obbligatorio'], Response::HTTP_INTERNAL_SERVER_ERROR);
  $response->send();
}

$complete = $request->query->get('complete', false);

$options = array(
  'location'      => $endpoint,
  'keep_alive'    => true,
  'trace'         => true,
  'local_cert'    => $certificate,
  'passphrase'    => $certificatePassword,
  'cache_wsdl'    => 0
);

try {
  $soapClient = new SoapClient($wsdl, $options);

  $xml = new SimpleXMLElement('<anag:RicercaImpresaPerCF xmlns:anag="http://InfoTn/2007/AnagraficaImprese" />');
  $xml->addChild('anag:User', $username);
  $xml->addChild('anag:Password', $password);
  $xml->addChild('anag:CodiceFiscale', $fiscalCode);
  $soapRequest = new SoapVar(trim(str_replace(array('<?xml version="1.0"?>'), '', $xml->asXML())), XSD_ANYXML );
  $result = $soapClient->RicercaImpresePerCF($soapRequest);


  if (property_exists($result, 'HEADER')) {
    if (property_exists($result->HEADER, 'ESITO') && $result->HEADER->ESITO === 'OK') {

      // Continuo con la richiesta di dettaglio
      if (is_array($result->DATI->LISTA_IMPRESE->ESTREMI_IMPRESA)) {
        $rea = $result->DATI->LISTA_IMPRESE->ESTREMI_IMPRESA;
      } else {
        $rea = [$result->DATI->LISTA_IMPRESE->ESTREMI_IMPRESA];
      }

      $data = [];
      foreach ($rea as $r) {
        $detail = ParixRequest::getDetail($soapClient, $username, $password, $r->DATI_ISCRIZIONE_REA, $complete);

        if ($detail['status'] === 'success') {
          $data[]= $detail['data'];
        } else {
          $data[]= $detail;
        }
      }
      $response = new JsonResponse($data, Response::HTTP_OK);
      $response->send();

    } else {
      $response = new JsonResponse($result->DATI->ERRORE, Response::HTTP_NOT_FOUND);
      $response->send();
    }
  } else {
    $response = new JsonResponse(['Si è verificato un errore'], Response::HTTP_INTERNAL_SERVER_ERROR);
    $response->send();
  }


} catch(Exception $e) {
  $response = new JsonResponse([$e->getMessage()], Response::HTTP_INTERNAL_SERVER_ERROR);
  $response->send();
}
