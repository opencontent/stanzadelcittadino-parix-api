<?php

require 'vendor/autoload.php';

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;

$response = new JsonResponse(['System is alive'], Response::HTTP_NOT_FOUND);
$response->send();
