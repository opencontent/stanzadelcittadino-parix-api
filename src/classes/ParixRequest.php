<?php

namespace App;

use SimpleXMLElement;
use SoapVar;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;

class ParixRequest
{

  public static function getDetail($soapClient, $username, $password, $rea, $complete)
  {
    if (!is_array($rea)) {
      $numRea = $rea->NREA;
      $province = $rea->CCIAA;
    } else {
      $numRea = $rea[0]->NREA;
      $province = $rea[0]->CCIAA;
    }

    $xml = new SimpleXMLElement('<anag:DettaglioImpresa xmlns:anag="http://InfoTn/2007/AnagraficaImprese" />');
    $xml->addChild('anag:User', $username);
    $xml->addChild('anag:Password', $password);
    $xml->addChild('anag:ProvinciaSede', $province);
    $xml->addChild('anag:NumeroREASede', $numRea);
    $soapRequest = new SoapVar(trim(str_replace(array('<?xml version="1.0"?>'), '', $xml->asXML())), XSD_ANYXML );

    if ($complete) {
      $result = $soapClient->DettaglioCompletoImpresa($soapRequest);
    } else {
      $result = $soapClient->DettaglioRidottoImpresa($soapRequest);
    }

    if (property_exists($result, 'HEADER')) {
      if (property_exists($result->HEADER, 'ESITO') && $result->HEADER->ESITO === 'OK') {
        return [
          'status' => 'success',
          'data' => $result
        ];

      } else {
        return [
          'status' => 'error',
          'message' => $result->DATI->ERRORE
        ];
      }
    } else {
      return [
        'status' => 'error',
        'message' => 'Si è verificato un errore'
      ];
    }
  }
}
